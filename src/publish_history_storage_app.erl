%%%-------------------------------------------------------------------
%%% @author XuLei
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 15. 八月 2016 19:18
%%%-------------------------------------------------------------------
-module(publish_history_storage_app).
-author("XuLei").

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%%===================================================================
%%% Application callbacks
%%%===================================================================
start(_Type, _StartArgs) ->
    case publish_history_storage_sup:start_link() of
        {ok, Pid} ->
            {ok, Pid};
        Other ->
            {error, Other}
    end.

stop(_State) ->
    ok.
%%%-------------------------------------------------------------------
%%% @author XuLei
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. 八月 2016 19:06
%%%-------------------------------------------------------------------
-module(publish_history_storage).
-author("XuLei").

-include("publish_history_storage.hrl").

-behaviour(gen_server).

%% API
-export([
    start_link/0, 
    store/1
    ]).

%% gen_server callbacks
-export([
    init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3
    ]).

-define(SERVER, ?MODULE).

-record(state, {mysql_pid :: pid()}).

-compile({
    parse_transform, 
    lager_transform
    }).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
%% -spec(start_link() ->
%%   {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
%% -spec(init(Args :: term()) ->
%%    {ok, State :: #state{}} |
%%    {ok, State :: #state{}, timeout() | hibernate} |
%%    {stop, Reason :: term()} | ignore).
init([]) ->
    {ok, Mysql_pid} = link_mysql(),
    ok = mysql:query(Mysql_pid,<<"create table if not exists publish_history_storage(
                                                                                        message_id varbinary(3000),
                                                                                        appkey varbinary(3000),
                                                                                        uid varbinary(3000),
                                                                                        topic varbinary(3000), 
                                                                                        payload varbinary(3000), 
                                                                                        timestamp varbinary(3000)
                                                                                        )">>),
    %store to state
    State = #state{mysql_pid = Mysql_pid},
    {ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
    {reply, Reply :: term(), NewState :: #state{}} |
    {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_call(_Request, _From, State) ->
    {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(Message, State) ->
    Message_after_process = process_message(Message),
    Mysql_pid = State#state.mysql_pid,
    case mysql:query(Mysql_pid, <<"insert publish_history_storage value(?,?,?,?,?,?)">>, [
                                                                                            Message_after_process#message.message_id,
                                                                                            Message_after_process#message.appkey,
                                                                                            Message_after_process#message.uid,
                                                                                            Message_after_process#message.topic,
                                                                                            Message_after_process#message.payload, 
                                                                                            Message_after_process#message.timestamp
                                                                                            ]) of
        ok ->
            ok;
      %if erro occurs, stop the application and restart and save the erro info in the log
        {error, Reason} ->
            lager:debug("handle_cast error occurs:~n Mysql_pid:~p~n Message_after_process:~p~n", [Mysql_pid, Message_after_process]),
            ok = application:stop(publish_history_storage)
    end,
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
      Extra :: term()) ->
    {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

link_mysql() ->
    {ok, Mysql_config} = application:get_env(publish_history_storage, mysql_connection),
    lager:debug("mysql_config: ~p~n", [Mysql_config]),
    Host = proplists:get_value(host, Mysql_config),
    User = proplists:get_value(user, Mysql_config),
    Database = proplists:get_value(database, Mysql_config),
    Password = proplists:get_value(password, Mysql_config),
    {ok, Mysql_pid} = mysql:start_link([{host, Host},{user, User},{password, Password},{database, Database}]),
    {ok, Mysql_pid}.

store(Message) ->
    gen_server:cast(?SERVER, Message).


%change the message from unbinary to binary
%if unfit message is sent, record the erro and dont store it
process_message(Message) ->
  %it should handle too_long first, then is not binary
    Binary_message = process_not_binary_message(Message),
    Not_too_long_message = process_too_long_message(Binary_message),
    Not_too_long_message.

process_not_binary_message(Message) ->
    Message_id = case is_binary(Message#message.message_id) of
                        true ->  Message#message.message_id;
                        false -> term_to_binary(Message#message.message_id)
                 end,
    Appkey = case is_binary(Message#message.appkey) of
                    true ->  Message#message.appkey;
                    false -> term_to_binary(Message#message.appkey)
             end,
    Uid = case is_binary(Message#message.uid) of
                true ->  Message#message.uid;
                false -> term_to_binary(Message#message.uid)
          end,
    Topic = case is_binary(Message#message.topic) of
                    true ->  Message#message.topic;
                    false -> term_to_binary(Message#message.topic)
            end,
    Payload = case is_binary(Message#message.payload) of
                    true ->  Message#message.payload;
                    false ->  term_to_binary(Message#message.payload)
              end,
    Timestamp = case is_binary(Message#message.timestamp) of
                        true ->  Message#message.timestamp;
                        false ->  term_to_binary(Message#message.timestamp)
                end,
    Message_after_process = #message{
                                        message_id = Message_id, 
                                        appkey = Appkey, 
                                        uid = Uid, 
                                        topic = Topic, 
                                        payload = Payload, 
                                        timestamp = Timestamp
                                        },
    Message_after_process.
  
process_too_long_message(Message) ->
    Message_id = case byte_size(Message#message.message_id) < 2990 of
                        true ->  Message#message.message_id;
                        false -> <<"too_long">>
               end,
    Appkey = case byte_size(Message#message.appkey) < 2990 of
                    true ->  Message#message.appkey;
                    false -> <<"too_long">>
          end,
    Uid = case byte_size(Message#message.uid) < 2990 of
                true ->  Message#message.uid;
                false -> <<"too_long">>
          end,
    Topic = case byte_size(Message#message.topic) < 2990 of
                    true ->  Message#message.topic;
                    false -> <<"too_long">>
            end,
    Payload = case byte_size(Message#message.payload) < 2990 of
                    true ->  Message#message.payload;
                    false ->  <<"too_long">>
              end,
    Timestamp = case byte_size(Message#message.timestamp) < 2990 of
                        true ->  Message#message.timestamp;
                        false ->  <<"too_long">>
            end,
    Message_after_process = #message{
                                        message_id = Message_id, 
                                        uid = Uid, appkey = Appkey, 
                                        topic = Topic, 
                                        payload = Payload, 
                                        timestamp = Timestamp
                                        },
    Message_after_process.
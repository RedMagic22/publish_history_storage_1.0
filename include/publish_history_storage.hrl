%%%-------------------------------------------------------------------
%%% @author XuLei
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. 八月 2016 19:10
%%%-------------------------------------------------------------------
-author("xulei").

-record(message, {
  message_id  :: binary(),
  appkey      :: binary(),
  uid         :: binary(),
  topic       :: binary(),
  payload     :: binary(),
  timestamp   :: binary()
}).

%all the information must be string, for example: Mysql_login_information = #mysql_login_information{host = "localhost", user = "root", password = "12345678", database = "database1"}
-record(mysql_login_information, {
	host	 :: list(),
	user	 :: list(),
	password :: list(),
	database :: list()
}).
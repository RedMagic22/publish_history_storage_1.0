%%%-------------------------------------------------------------------
%%% @author XuLei
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 20. 八月 2016 下午5:26
%%%-------------------------------------------------------------------

-module(mysql_publish_test).
-author("XuLei").

-export([all_test/0]).

-export([
    handle_usual_test/0, 
    handle_not_binary_message_test/0, 
    handle_too_long_message_test/0, 
    handle_rpc_test/0
    ]).
    
-include("publish_history_storage.hrl").


all_test() ->
    handle_usual_test(),
    io:format("usual test passed~n"),
    handle_not_binary_message_test(),
    io:format("not binary messsage test passed~n"),
    handle_too_long_message_test(),
    io:format("too long message test passed~n~n"),
    
    receive 
        wait -> suspend
    after
        3000 -> start
    end,
    
    handle_rpc_test(),
    io:format("rpc test passed~n"),
    'all test passed'.

handle_usual_test() ->
    {ok, Mysql_pid} = application:get_env(publish_history_storage, mysql_pid),
    ok = mysql:query(Mysql_pid,<<"create table if not exists publish_history_storage(
                                                                                        message_id varbinary(3000),
                                                                                        appkey varbinary(3000),
                                                                                        uid varbinary(3000),
                                                                                        topic varbinary(3000),
                                                                                        payload varbinary(3000),
                                                                                        timestamp varbinary(3000)
                                                                                        )">>),
    ok = mysql:query(Mysql_pid, <<"delete from publish_history_storage where message_id = 'test_message_id'">>),
    Mysql_publish_args = #message{
                                    message_id = <<"test_message_id">>, 
                                    appkey = <<"test_appkey">>, uid = <<"test_uid">>, 
                                    topic = <<"test_topic">>, 
                                    payload = <<"test_payload">>, 
                                    timestamp = <<"test_timestamp">>
                                    },
    publish_history_storage:store(Mysql_publish_args),

    receive 
        wait -> suspend
    after
        1000 -> start
    end,

    {ok, Colunm, Rows} = mysql:query(Mysql_pid, <<"select * from publish_history_storage where message_id = 'test_message_id'">>),
    io:format("~n Colunm is:~w~n", [Colunm]),
    io:format("~n Rows is:~w~n",[Rows]),
    [
        <<"message_id">>,
        <<"appkey">>,
        <<"uid">>,
        <<"topic">>,
        <<"payload">>,
        <<"timestamp">>
        ] = Colunm,
    [[
        <<"test_message_id">>,
        <<"test_appkey">>,
        <<"test_uid">>,
        <<"test_topic">>,
        <<"test_payload">>,
        <<"test_timestamp">>
        ]] = Rows,
    ok = mysql:query(Mysql_pid, <<"delete from publish_history_storage where message_id = 'test_message_id'">>),
    'usual test passed'.

handle_not_binary_message_test() ->
    {ok, Mysql_pid} = application:get_env(publish_history_storage, mysql_pid),
    ok = mysql:query(Mysql_pid,<<"create table if not exists publish_history_storage(
                                                                                        message_id varbinary(3000),
                                                                                        appkey varbinary(3000),
                                                                                        uid varbinary(3000),
                                                                                        topic varbinary(3000),
                                                                                        payload varbinary(3000),
                                                                                        timestamp varbinary(3000)
                                                                                        )">>),
    ok = mysql:query(Mysql_pid, <<"delete from publish_history_storage where message_id = 'not_binary'">>),
    Mysql_publish_args = #message{
                                    message_id = "not_binary_message", 
                                    appkey = not_binary_message, 
                                    uid = {no, binary, message}, 
                                    topic = not_binary_message, 
                                    payload = {n, b, m}, 
                                    timestamp = 123456
                                    },
    publish_history_storage:store(Mysql_publish_args),

    receive 
        wait -> suspend
    after
        1000 -> start
    end,

    {ok, Colunm, Rows} = mysql:query(Mysql_pid, <<"select * from publish_history_storage where message_id = 'not_binary'">>),
    [
        <<"message_id">>,
        <<"appkey">>,
        <<"uid">>,
        <<"topic">>,
        <<"payload">>,
        <<"timestamp">>
        ] = Colunm,
    [[
        <<"not_binary">>,
        <<"not_binary">>,
        <<"not_binary">>,
        <<"not_binary">>,
        <<"not_binary">>,
        <<"not_binary">>
        ]] = Rows,
    ok = mysql:query(Mysql_pid, <<"delete from publish_history_storage where message_id = 'not_binary'">>),
    'not binary message test passed'.

handle_too_long_message_test() ->
    {ok, Mysql_pid} = application:get_env(publish_history_storage, mysql_pid),
    ok = mysql:query(Mysql_pid,<<"create table if not exists publish_history_storage(
                                                                                        message_id varbinary(3000),
                                                                                        appkey varbinary(3000),
                                                                                        uid varbinary(3000),
                                                                                        topic varbinary(3000),
                                                                                        payload varbinary(3000),
                                                                                        timestamp varbinary(3000)
                                                                                        )">>),
    ok = mysql:query(Mysql_pid, <<"delete from publish_history_storage where message_id = 'too_long'">>),
    Mysql_publish_args = #message{
                                    message_id = <<1:40000>>, 
                                    appkey = <<1:40000>>, 
                                    uid = <<1:40000>>, 
                                    topic = <<1:40000>>, 
                                    payload = <<1:40000>>, 
                                    timestamp = <<1:40000>>
                                    },
    publish_history_storage:store(Mysql_publish_args),

    receive 
        wait -> suspend
    after
        1000 -> start
    end,

    {ok, Colunm, Rows} = mysql:query(Mysql_pid, <<"select * from publish_history_storage where message_id = 'too_long'">>),
    [
        <<"message_id">>,
        <<"appkey">>,
        <<"uid">>,
        <<"topic">>,
        <<"payload">>,
        <<"timestamp">>
        ] = Colunm,
    [[
        <<"too_long">>,
        <<"too_long">>,
        <<"too_long">>,
        <<"too_long">>,
        <<"too_long">>,
        <<"too_long">>
        ]] = Rows,
    ok = mysql:query(Mysql_pid, <<"delete from publish_history_storage where message_id = 'too_long'">>),
    'too long message test passed'.


handle_rpc_test() ->
    {ok, Node} = io:read("Enter a linked Node to test rpc like 'publish@127.0.0.1' and end up with a period:"),
    pong = net_adm:ping(Node),
%   Mysql_publish_args = #message{msgid = <<"test_mysql_publish">>, topic = <<"test_topic">>, payload = <<"test_payload">>, date = <<"test_date">>},
  
    {ok, Mysql_config} = rpc:call(Node, application, get_env, [publish_history_storage, mysql_connection]),
  
    Host = proplists:get_value(host, Mysql_config),
    User = proplists:get_value(user, Mysql_config),
    Database = proplists:get_value(database, Mysql_config),
    Password = proplists:get_value(password, Mysql_config),
%   rpc:cast(Node, publish_history_storage, store, [Mysql_publish_args, Mysql_login_information]),
    {ok, _Mysql_pid} = rpc:call(Node, mysql, 'start_link', [[{host, Host},{user, User},{password, Password},{database, Database}]]),
%   rpc:call(Node, mysql, 'query', [_Mysql_pid, <<"select * from publish_history_storage where msgid = 'test_mysql_publish'">>]),
%   [<<"msgid">>, <<"topic">>, <<"payload">>, <<"date">>] = Colunm,
%   [[<<"test_mysql_publish">>,<<"test_topic">>,<<"test_payload">>,<<"test_date">>]] = Rows,
%    rpc:call(Node, mysql, 'query', [Mysql_pid, <<"delete from publish_history_storage where msgid = 'test_mysql_publish'">>]),

    'rpc test passed'.
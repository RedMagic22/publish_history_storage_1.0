.PHONY: all deps clean release

all: compile

compile: deps
	./rebar -j8 compile

deps:
	./rebar -j8 get-deps

clean:
	./rebar -j8 clean

relclean:
	rm -rf rel/publish_history_storage

generate: compile
	cd rel && .././rebar -j8 generate

run: generate
	./rel/publish_history_storage/bin/publish_history_storage start

console: generate
	./rel/publish_history_storage/bin/publish_history_storage console

foreground: generate
	./rel/publish_history_storage/bin/publish_history_storage foreground

erl: compile
	erl -pa ebin/ -pa deps/*/ebin/ -s publish_history_storage

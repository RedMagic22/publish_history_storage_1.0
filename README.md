## Overview
---

`publish_history_storage`是一个模块，调用它可以将传入参数存入mysql中。若传入的参数是publish消息，则可以实现publish history的功能。

## Configuration
---

使用前需要修改`rel/files/app.config`文件内的`publish_history_storage`项下`mysql_connection`中mysql的`host`，`user`，`password`，`database`使得可以正确连接到数据库。

`publish_history_storage`对外呈现一个API，如下所示：

`publish_history_storage:store/1`

其中`store`需要一个参数`message`，这是一个record，详见`include/publish_history_storage.hrl`:

注意这里的message的各个字段的长度都要确定，否则不能成功调用。

```
-record(message, {
    message_id   :: binary(),
    appkey       ::bianry(),
    uid          ::bianry()
    topic        :: binary(),
    payload      :: binary(),
    timestamp    :: binary()
}).

```

## Build and Start
___

	make console

## Test
___

	mysql_publish_test:all_test().

##Design
---

使用该模块，将连接到配置文件`rel/files/app.config`中所选定的mysql数据库下面，并新建一个`publish_history_storage`的表，表结构如下所示：

| Field        | Type            | Null | Key | Default | Extra |
|--------------|-----------------|------|-----|---------|-------|
| message_id   | varbinary(3000) | YES  |     | NULL    |       |
| appkey       | varbinary(3000) | YES  |     | NULL    |       |
| uid          | varbinary(3000) | YES  |     | NULL    |       |
| topic        | varbinary(3000) | YES  |     | NULL    |       |
| payload      | varbinary(3000) | YES  |     | NULL    |       |
| timestamp    | varbinary(3000) | YES  |     | NULL    |       |

并将传入的参数存入这个表中。

若传入的record中的字段不是`binary()`，将会以用`term_to_bianry(Message)`转换成`bianry()`存入表中。（注意：这样转换出来的数据由于格式和mysql不一致，所以在mysql中直接查询会显示乱码，只需要从mysql中取出来之后，使用bianry_to_term转换回去）

若传入的record中的字段大于3000字节，将会以`too_long`的结果存入mysql。